package spaceinvade;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Ini;


public class InterfaceSpaceInvaders extends JFrame{
	static int score = 0;
	private final Color[][] g;
	private Color background = Color.BLACK;
	private final Graphic p;
	private final int bordure = 1,cell = 3;
	private KeyEvent ke;
	private final JLabel Score, Alive, Lifes, Niveau;
         private final JPanel panel1;
         
	private class KEDispatcher implements KeyEventDispatcher { 
                @Override
		public boolean dispatchKeyEvent(KeyEvent e) {
			if (e.getID() == KeyEvent.KEY_PRESSED) {
				if(e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_A || e.getKeyCode() == KeyEvent.VK_Z ||  e.getKeyCode() == KeyEvent.VK_E || e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_PAUSE || e.getKeyCode() == KeyEvent.VK_SPACE) ke = e;
			}
			return false;
		}
	}
	 
	public InterfaceSpaceInvaders(int largeur, int hauteur, Color fond){
		super("Space Invaders");
		this.g = new Color[largeur][hauteur];
		for(int i=0;i<this.g.length;i++){
			for(int j=0;j<this.g[i].length;j++){
				this.g[i][j] = background;
			}
		}
		this.background = fond;
		this.p = new Graphic();
		this.p.setPreferredSize(new Dimension(2*this.bordure+(this.g.length-1)*this.cell, 2*this.bordure+(this.g[0].length-1)*this.cell));
		this.getContentPane().add(this.p);
                
                 panel1 = new JPanel();
                 panel1.setBackground(Color.black);

                this.Score = new JLabel();              
                Score.setFont(new Font("Comic Sans Ms", Font.PLAIN, 20));
                Score.setForeground(Color.green);
                Score.setBackground(Color.black);
                Score.setOpaque(true);
                
                this.Alive = new JLabel();              
                Alive.setFont(new Font("Comic Sans Ms", Font.PLAIN, 20));
                Alive.setForeground(Color.green);
                Alive.setBackground(Color.black);
                Alive.setOpaque(true);
                
                this.Lifes = new JLabel();              
                Lifes.setFont(new Font("Comic Sans Ms", Font.PLAIN, 20));
                Lifes.setForeground(Color.green);
                Lifes.setBackground(Color.black);
                Lifes.setOpaque(true);
                
                this.Niveau = new JLabel();              
                Niveau.setFont(new Font("Comic Sans Ms", Font.PLAIN, 20));
                Niveau.setForeground(Color.green);
                Niveau.setBackground(Color.black);
                Niveau.setOpaque(true);
                
                panel1.add (this.Alive);
                panel1.add (this.Lifes);
                panel1.add (this.Score);
                
                
                
                this.getContentPane().add(this.panel1,BorderLayout.PAGE_START);
                  this.getContentPane().add(this.Niveau,BorderLayout.PAGE_END);
                
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new KEDispatcher());
	}
	
	public void modifieCase(int x, int y, Color c){
		if(x>-1 && x<this.g.length && y>-1 && y<this.g[0].length) g[x][y] = c;
		this.p.repaint();
	}

	public void effaceCase(int x, int y){
		if(x>-1 && x<this.g.length && y>-1 && y<this.g[0].length) g[x][y] = this.background;
		this.p.repaint();
	}
	public void UpdateScore(int valeur){
                score = score + valeur;
		this.Score.setText( " Score : " + Integer.toString(score));
              
	}
       public void UpdateAlive(int valeur){
		this.Alive.setText( " Monstres restants : " + Integer.toString(valeur) + " || ");
              
	}
       public void UpdateLifes(int valeur){
		this.Lifes.setText( " Vies restantes : " + Integer.toString(valeur) + " || ");
              
	}
      public void UpdateNiveau(int valeur){
		this.Niveau.setText( " Niveau : " + Integer.toString(valeur));
              
	}
        public void ligneCase(int x,int y, int xf, Color c){
            int l=0;
            for(int i =0;i<=xf;i++)
            {
		if(x+l>-1 && x+l<this.g.length) g[x+l][y] = c;
                l=l+1;
        }
		this.p.repaint();
	}
        public void DrawAlien(int x,int y,Color c)
        {
        this.modifieCase(x, y, c);
        this.modifieCase(x+1, y+1, c);
        this.modifieCase(x+6, y, c);
        this.modifieCase(x+5, y+1, c);
        this.ligneCase(x, y+2,6, c);
        this.ligneCase(x-1, y+3,1, c);
        this.ligneCase(x+2, y+3,2, c);
        this.ligneCase(x+6, y+3,1, c);
        this.ligneCase(x-2, y+4,10, c);
        
        //bras gauche
        this.modifieCase(x-2, y+5, c);
        this.modifieCase(x-2, y+6, c);
        
        
        this.ligneCase(x, y+5,6, c);
        
        //bras droit
        this.modifieCase(x+8, y+5, c);
        this.modifieCase(x+8, y+6, c);
        
        
        //jambe gauche
        this.modifieCase(x, y+6, c);
        this.modifieCase(x+1, y+7, c);
        
        // jambe droite
        this.modifieCase(x+6, y+6, c);
        this.modifieCase(x+5, y+7, c);
         }
        
        public void DrawAliens(int x,int y, Color c, int h, int v)
        {
            int space = 20;
            int spaceh = 0;
            for (int i =0; i<=h;i++)
            {
            
                this.DrawAlien(x+space,y,c);
                space = space + 20;
                
            }
            
        }
        public void SupprAlien(int x,int y)
        {
        
            this.modifieCase(x, y, this.background);
        this.modifieCase(x+1, y+1, this.background);
        this.modifieCase(x+6, y, this.background);
        this.modifieCase(x+5, y+1, this.background);
        this.ligneCase(x, y+2,6, this.background);
        this.ligneCase(x-1, y+3,1, this.background);
        this.ligneCase(x+2, y+3,2, this.background);
        this.ligneCase(x+6, y+3,1, this.background);
        this.ligneCase(x-2, y+4,10, this.background);
        
        //bras gauche
        this.modifieCase(x-2, y+5, this.background);
        this.modifieCase(x-2, y+6, this.background);
        
        
        this.ligneCase(x, y+5,6, this.background);
        
        //bras droit
        this.modifieCase(x+8, y+5, this.background);
        this.modifieCase(x+8, y+6, this.background);
        
        
        //jambe gauche
        this.modifieCase(x, y+6, this.background);
        this.modifieCase(x+1, y+7, this.background);
        
        // jambe droite
        this.modifieCase(x+6, y+6, this.background);
        this.modifieCase(x+5, y+7, this.background);
        }
        public void DrawPlayer(int x,int y, Color c)
        {
        
         this.ligneCase(x+5, y-1,2, c);
         this.ligneCase(x+5, y,2, c);   
         this.ligneCase(x+3, y+1,6, c);
         this.ligneCase(x+2, y+2,8, c);
         this.ligneCase(x+2, y+3,8, c);
         this.ligneCase(x+2, y+4,8, c);
        }
        public void SupprPlayer(int x,int y)
        {
        
         this.ligneCase(x+5, y-1,2, this.background);
         this.ligneCase(x+5, y,2, this.background);   
         this.ligneCase(x+3, y+1,6, this.background);
         this.ligneCase(x+2, y+2,8, this.background);
         this.ligneCase(x+2, y+3,8, this.background);
         this.ligneCase(x+2, y+4,8, this.background);
        }

        public void DrawBullet(int x, int y, Color color)
        {
        this.modifieCase(x+6, y-2, color);
        this.modifieCase(x+6, y-3, color);
        this.modifieCase(x+6, y-4, color);
        }
       public void DrawProtection(int x, int y, Color color)
        {
        this.ligneCase(x,y,10, color);
      
        }
       public void SupprBullet(int x,int y)
        {
        
        this.effaceCase(x+6, y-2);
        this.effaceCase(x+6, y-3);
         this.effaceCase(x+6, y-4);
        }
	public int toucheTapee(){
		if(this.ke == null) return -1;
		else{
			int result = -1;
                        if(this.ke.getKeyCode() == KeyEvent.VK_A) result = 6;
                        if(this.ke.getKeyCode() == KeyEvent.VK_Z) result = 7;
                         if(this.ke.getKeyCode() == KeyEvent.VK_E) result = 8;
			if(this.ke.getKeyCode() == KeyEvent.VK_SPACE) result = 0;
			if(this.ke.getKeyCode() == KeyEvent.VK_RIGHT) result = 2;
			if(this.ke.getKeyCode() == KeyEvent.VK_LEFT) result = 1;
			if(this.ke.getKeyCode() == KeyEvent.VK_DOWN) result = 3;
			if(this.ke.getKeyCode() == KeyEvent.VK_UP) result = 4;
			if(this.ke.getKeyCode() == KeyEvent.VK_PAUSE) result = 5;
			this.ke = null;
			return result;
		}
	}
	
	private class Graphic extends JPanel{
		public void paint(Graphics gr){
			gr.setColor(background);
			gr.fillRect(0,0,this.getWidth(),this.getHeight());
			for(int i=0;i<g.length;i++){
				for(int j=0;j<g[i].length;j++){
					gr.setColor(g[i][j]);
					gr.fillRect(bordure+cell*i,bordure+cell*j,cell,cell);
				}
			}
		}
	}
	public void sauvegarde(int niveau, String nom)
        {
                      Ini ini = null;
        try {
            ini = new Ini(new File("reglages.ini"));
        } catch (IOException ex) {
            Logger.getLogger(Single.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         for(int i =1; i <= 10;i++)
        {
             String test = ini.get("Savedgame" + i, "Niveau");
             
            if(test == null)
            {
                 ini.put("Savedgame" +i, "Niveau", niveau);
                 ini.put("Savedgame" +i, "Nom", nom);
                 try {
                     ini.store();
                 } catch (IOException ex) {
                     Logger.getLogger(InterfaceSpaceInvaders.class.getName()).log(Level.SEVERE, null, ex);
                 }
                 break;
            }                                             
            
        }
         System.exit(0);
         
        }
	/** Affiche le message m dans une boite de dialogue. */
	public void afficheMessage(int niveau, String nom){
		int reponse = JOptionPane.showConfirmDialog(this,"Voulez vous sauvegarder?","Game Over",
    JOptionPane.YES_NO_OPTION);
                if (reponse == JOptionPane.YES_OPTION)
                    sauvegarde(niveau, nom);
                else
                    System.exit(0);
	}
        public void afficheMessage2(String nom){
		JOptionPane.showMessageDialog(this,nom + "a gagné");
	}
}