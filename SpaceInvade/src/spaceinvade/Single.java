
package spaceinvade;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import static spaceinvade.Menu.setup;
import org.ini4j.Ini;
import static spaceinvade.Menu.mode;

/**
 *
 * @author PIERRE
 */
public class Single extends javax.swing.JFrame {

    private String lvl;

    public Single() {
        initComponents();
        loadini();
    }
    @SuppressWarnings("unchecked")
    private void loadini()  
    {
        
         LinkedList<String> linkedlist = new LinkedList<String>();
                    

           Ini ini = null;
        try {
            ini = new Ini(new File("reglages.ini"));
        
            linkedlist.add("Nouvelle Partie");
        for(int i =1; i <= 30;i++)
        {
             String niveau = ini.get("Savedgame" + i, "Niveau");
             String nm = ini.get("Savedgame" + i, "Nom");
             
            if(niveau != null)
            {
                     linkedlist.add("Sauvegarde niveau : " + niveau + " Joueur : " + nm);
                     lvl = niveau;
            }
            
           savegamebox.setModel(new DefaultComboBoxModel(linkedlist.toArray()));
        }
        
        } catch (IOException ex) {
           
        }
        
    

               
           
    }
    
       private boolean GetInformations() 
       {
        
       switch(difficultybox.getSelectedIndex())
       {
           case 1:  Menu.difficulty = 1;
           break;
           case 2: Menu.difficulty = 2;
           break; 
           case 3: Menu.difficulty = 3;
           break; 
       }
           
          switch(colorpbox.getSelectedIndex())
       {
           case 1:  Menu.colorp = Color.green;
           break;
           case 2: Menu.colorp = Color.red;
           break; 
           case 3: Menu.colorp = Color.blue;
           break; 
       }
           switch(colorebox.getSelectedIndex())
       {
           case 1:  Menu.colore = Color.green;
           break;
           case 2: Menu.colore = Color.red;
           break; 
           case 3: Menu.colore = Color.blue;
           break; 
       }
           
         if(savegamebox.getSelectedIndex() == 0)
             Menu.niveau = "1";
         else
         {
      
         Menu.niveau = lvl;
         }

         Menu.nom = jTextField1.getText();
         return true;
        
        
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        difficultybox = new javax.swing.JComboBox<>();
        savegamebox = new javax.swing.JComboBox<>();
        colorebox = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        colorpbox = new javax.swing.JComboBox<>();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jTextField1 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        difficultybox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Difficulté", "Facile", "Moyen", "Difficile" }));

        savegamebox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nouvelle Partie" }));
        savegamebox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savegameboxActionPerformed(evt);
            }
        });

        colorebox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Couleur envahisseurs", "Vert", "Rouge", "Bleu" }));

        jButton1.setText("Jouer");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        colorpbox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Couleur joueur", "Vert", "Rouge", "Bleu" }));
        colorpbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorpboxActionPerformed(evt);
            }
        });

        jTextField1.setText("Nom");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator2)
                            .addComponent(jSeparator1)
                            .addComponent(jSeparator3)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(51, 51, 51)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(colorebox, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(difficultybox, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(63, 63, 63)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addComponent(colorpbox, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(51, 51, 51)
                                .addComponent(savegamebox, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 15, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(difficultybox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(savegamebox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(colorebox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorpbox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
       if(GetInformations());
       {
        setup = true;
        mode = 1;
       }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void colorpboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorpboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_colorpboxActionPerformed

    private void savegameboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savegameboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_savegameboxActionPerformed


    public  void main(String args[])  {
                   java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                new Single().setVisible(true);
                        }
                        });
                
               

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> colorebox;
    private javax.swing.JComboBox<String> colorpbox;
    private javax.swing.JComboBox<String> difficultybox;
    private javax.swing.JButton jButton1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JComboBox<String> savegamebox;
    // End of variables declaration//GEN-END:variables
}
