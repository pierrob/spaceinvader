package spaceinvade;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.*;
import org.ini4j.Ini;

/**
 *
 * @author PIERRE
 */
public class SpaceInvade {


        static int score = 0; //INITIALISATION DU SCORE

        
	static class Alien  // CLASS ALIEN
	{
                int x;
                int y;
                boolean canmoveleft = true; //PEUT BOUGER A GAUCE (MULTIJOUEUR)
                boolean canmoveright = true; //PEUT BOUGER A DROITE (MULTIJOUEUR)
                boolean alive = true; //EN VIE
		boolean direction = true; //TRUE = GAUCHE ; FALSE = DROITE			
	}
       static class Player  // CLASS JOUEUR
	{		
                int x;
                int y;
		boolean canmoveleft = true; //PEUT BOUGER A GAUCE
                boolean canmoveright = true; //PEUT BOUGER A DROITE
                int live = 3; //VIES RESTANTES
	}
       static class Bullet  // CLASS BALLE
	{		
                int x;
                int y;
	}
       static class Bomb  // CLASS BOMB
	{		
                int x;
                int y;
	}
       
       	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  REGARDE SI LE JOUEUR TOUCHE UN ALIEN  ///////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
       static boolean hitbox(InterfaceSpaceInvaders Space,Alien[] alien, Bullet bullet)
       {
           int alienw = 8;
           int alienh = 9;
           for (int i = 0;i <= alien.length-1;i++)
           {
               if ( (bullet.x >= alien[i].x - alienw && bullet.x <= alien[i].x+alienw) && (bullet.y >= alien[i].y && alien[i].y+alienh >= bullet.y) && (alien[i].alive))
               {
                   Space.SupprAlien(alien[i].x, alien[i].y);
                   alien[i].alive = false;
                   Space.UpdateScore(20);
                   score = score + 20;
                    play("invaderkilled.wav");
                   return true;
               }
               
           }
           
           return false;
       }
       
       ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  REGARDE SI UNE BOMBE TOUCHE LES PROTECTIONS  ///////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static boolean hitboxprotection(InterfaceSpaceInvaders Space,Bomb bomb)
       {
           int protectionlenght = 9;
           int protection1x = 30;
           int protection2x = 100;
           int protection3x = 170;
           int protectiony = 150;
           
           return ( (  bomb.x >= protection1x -protectionlenght && bomb.x <= protection1x + protectionlenght) || ( bomb.x >= protection2x-protectionlenght && bomb.x <= protection2x + protectionlenght) || (bomb.x >= protection3x-protectionlenght && bomb.x <= protection3x + protectionlenght) ) && (bomb.y >= protectiony && protectiony+2 >= bomb.y);
       }
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  REGARDE SI UN MISSILE TOUCHE LES PROTECTIONS  ///////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
           static boolean hitboxprotectionp(InterfaceSpaceInvaders Space,Bullet bomb)
       {
           int protectionlenght = 9;
           int protection1x = 30;
           int protection2x = 100;
           int protection3x = 170;
           int protectiony = 150;
           
           return ( (  bomb.x >= protection1x -protectionlenght && bomb.x <= protection1x + protectionlenght) || ( bomb.x >= protection2x-protectionlenght && bomb.x <= protection2x + protectionlenght) || (bomb.x >= protection3x-protectionlenght && bomb.x <= protection3x + protectionlenght) ) && (bomb.y >= protectiony && protectiony+2 >= bomb.y);
       }
       
       	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  REGARDE SI UN ALIEN TOUCHE LE JOUEUR  ///////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static boolean hitboxbomb(InterfaceSpaceInvaders Space,Bomb bomb, Player play)
       {
           int playerw = 10;
           int playerh = 9;
           
               if ( (bomb.x >= play.x - 2 && bomb.x <= play.x+playerw) && (bomb.y >= play.y && play.y+playerh >= bomb.y))
               {
                   
                    Space.SupprPlayer(play.x, play.y);
                    play("explosion.wav");
                    play.live = play.live - 1;
                    Space.UpdateLifes(play.live);
                   return true;
               }
               
           
           return false;
       }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  JOUE LE SON  ///////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void play(String filename)
    {
    try
    {
        Clip clip = AudioSystem.getClip();
        clip.open(AudioSystem.getAudioInputStream(new File(filename)));
        clip.start();
    }
    catch (IOException | LineUnavailableException | UnsupportedAudioFileException exc)
    {
        exc.printStackTrace(System.out);
    }
    }
    
    	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  REGARDE SI LES ALIENS SONT EN DESSOUS DU JOUEUR  ////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static boolean alienreached(Alien[] alien)
    {
        for (int i =0;i <=alien.length-1;i++)
        {
            if(alien[i].y >= 170 && alien[i].alive)
                return true;
        }
        return false;
        
    }
    
    	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  GENERE UN NOMBRE ALEATOIRE PARMIS LES ALIENS VIVANTS  ///////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
public static int getRandom(int[] array, int length) {
    int rnd = new Random().nextInt(length);
    return array[rnd];
}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  GERE LA VITESSE SELON LE NIVEAU /////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int vitesseniveau (int niveau)
    {
        int sleep = 80 - (niveau * 4);
        
        return sleep;
    }
    
    
   	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  SAUVEGARDE LE SCORE  ///////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
     public static void sauvegarderscore (int scorew, String nom)
    {
               Ini ini = null;
        try {
            ini = new Ini(new File("reglages.ini"));
        } catch (IOException ex) {
            Logger.getLogger(Single.class.getName()).log(Level.SEVERE, null, ex);
        }
        
             String test = ini.get("Scores", nom);
             
             
         for(int i =1; i <= 30;i++)
        {
             String nomm = ini.get("Joueur", "nom"+i);
             
             if(nomm == null)
             {                  ini.put("Joueur", "nom"+i, nom);
                               break;
             }
             
             
        }
         
            if(test == null)
            {
                 ini.put("Scores", nom, scorew);
                  try {
                     ini.store();
                 } catch (IOException ex) {
                     Logger.getLogger(InterfaceSpaceInvaders.class.getName()).log(Level.SEVERE, null, ex);
                 }
                
            }
            else
                    {
                        if (Integer.parseInt(test) < scorew)
                        {
                            ini.put("Scores", nom, scorew);
                             try {
                     ini.store();
                 } catch (IOException ex) {
                     Logger.getLogger(InterfaceSpaceInvaders.class.getName()).log(Level.SEVERE, null, ex);
                 }
                        }
                            
                        
                    }           
           
           
    }
     	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  DEMARRE LE JEU  ///////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void Start(int diffulty,int niveau, Color colorp, Color colore, String nom) throws InterruptedException { 
        
        int aliennumber = 0;
        
        //NB D ALIEN sELON DIFFICULTE CHOISIE//
        switch (diffulty)
        {
            case 1:aliennumber = 6;
            break;
            case 2 : aliennumber = 12;
            break;
            case 3: aliennumber = 18;
            break;
        }
        ////////////////////////////////////////
        
        
        Alien alien[] = new Alien[aliennumber]; //iNITIALISATION DES ALIENS
        
        boolean finish = false;
        boolean gameover = false;
        
        boolean fire = false;
        boolean once = false;
        
        boolean bomb = false;
        boolean oncebomb = false;
        
        int x = 20;
        int y = 20;
        int monsteralive;  //NB DE MONSTRE RESTANTS
        
        
        
        ////////// CREER LES LIGNES D ALIENS////////////
        for(int i = 0; i<=alien.length-1;i++)
        {
               if(i==6 ||i ==12)
               {
                 x = 20;
                 y = y +10;
               }
               
               
             alien[i]=new Alien();
             
             
             alien[i].x = x;
             
          
            alien[i].y = y;
             
             x = x+20;
             
        }
        ///////////////////////////////////////////////
        
        Player p = new Player();
        p.x = 95;                   //INITIALISATION DU JOUEUR
        p.y = 170;
        
        Bullet bullet = new Bullet();   //INITIALISATION DE LA BALLE
        
        Bomb bombdrop = new Bomb();     //INITIALISATION DE LA BOMBE
        
        InterfaceSpaceInvaders Space = new InterfaceSpaceInvaders(205, 200,Color.BLACK);

        Space.UpdateScore(0); //INITIALISE LE SCORE
        Space.UpdateLifes(p.live); //INITIALISE LES VIES
        Space.UpdateNiveau(niveau); //INITIALISE LE NIVEAU
        Space.DrawProtection(30, 150, colorp);
        Space.DrawProtection(100, 150, colorp);
        Space.DrawProtection(170, 150, colorp);
        
        int first = 0; //INITIALISE LE PREMIER ALIEN DE LA LIGNE
        int last = 0; //INITIALISE LE DERNIER ALIEN DE LA LIGNE
        
        int sleep = vitesseniveau(niveau); //VITESSE DU NIVEAU
        
        
        
        
        while(!finish)  //BOUCLE DE JEU
        {
            
            //DESSINE LES ALIENS/////////////////////////////
            for (int i =0; i<= alien.length-1;i++)
            {
               if (alien[i].alive)
              Space.DrawAlien(alien[i].x, alien[i].y,colore);
            }
            /////////////////////////////////////////////////
            
            Space.DrawPlayer(p.x, p.y, colorp); //DESSINE LE JOUEUR
            
               
            //INDIQUE LE PREMIER ET DERNIER ALIEN DE LA LIGNE POUR LES CHANGEMENTS DE DIRECTIONS//
            for (int j =0;j<=5;j++)
            {
                if (alien[j].alive)
                {
                    first = j;
                    break;}
            }
            
            for (int j =5;j>=0;j--)
            {
                if (alien[j].alive)
                {
                    last = j;
                    
                    break;}
            }
            /////////////////////////////////////////////////////////////////////////////////////
            
            
            //CHANGEMENT DE DIRECTIONS//////////////////////////////////////////////////////////
            if (alien[first].x <= 5)
            {
            alien[first].direction = false;//DROITE
            
             for(int i =0; i<=alien.length-1;i++)
                        {
                    
                            alien[i].y = alien[i].y +2;
                        }
            }
            else if (alien[last].x >= 190)
            {
                alien[first].direction = true;//GAUCHE
                 for(int i =0; i<=alien.length-1;i++)
                        {
                  
                            alien[i].y = alien[i].y +2;
                        }
            }
            
            ////////////////////////////////////////////////////////////////////////////////////
            
            //DETECTION DES BORDS POUR LE JOUEUR////////////////////////////////////////////////
            
            switch (p.x) {
                case 5:
                    p.canmoveleft = false;
                    break;
                case 185:
                    p.canmoveright = false;
                    break;
                default:
                    p.canmoveright = true;
                    p.canmoveleft = true;
                    break;
            }
            //////////////////////////////////////////////////////////////////////////////////////
            
            //MOUVEMENTS DES ALIENS///////////////////////////////////////////////////////////////
            if (alien[first].direction)
            {
                        Thread.sleep(sleep);
                        
                        for(int i =0; i<=alien.length-1;i++)
                        {
                            Space.SupprAlien(alien[i].x,  alien[i].y-2);
                            Space.SupprAlien(alien[i].x,  alien[i].y);
                            alien[i].x = alien[i].x -5;
                        }
                                                                             
            }
            else 
            {
                        Thread.sleep(sleep);
                        for(int i =0; i<=alien.length-1;i++)
                        {
                            Space.SupprAlien(alien[i].x,  alien[i].y-2);
                            Space.SupprAlien(alien[i].x,  alien[i].y);
                            alien[i].x = alien[i].x +5;
                        }
            }           
            /////////////////////////////////////////////////////////////////////////////////////
            
            //ACTIVATION DU TIR/////////////////////////////////////////////////////////////////
            if (fire)
            { 
                if (!once) //on initialise le depart du tir
                {
                     bullet.x = p.x;
                     bullet.y = p.y;
                     play("shoot.wav");
                     once = true;
                }
                
                Space.SupprBullet(bullet.x, bullet.y);   //mouvement de la balle
                bullet.y = bullet.y -5;
                
                if(bullet.y <= 0 || hitbox(Space,alien,bullet) || hitboxprotectionp(Space,bullet))//si la balle est hors champs ou touche un alien
                {
                    Space.SupprBullet(bullet.x, bullet.y);
                    once = false;
                    fire = false;                  
                } 
                
            }
            //////////////////////////////////////////////////////////////////////////////////////
            
            
                
             if (fire)
              Space.DrawBullet(bullet.x,bullet.y,colorp); //mouvement de la balle
             
             
             //NOMBRE ALEATOIRE PARMIS LES ALIENS VIVANT POUR LE LACHER DE BOMBES
             int alive[] = new int[alien.length];
             int alivecount = 0;
             for (int i = 0;i<=alien.length-1;i++)
             {
                 if(alien[i].alive)
                 {
                     alive[alivecount] = i;
                     alivecount = alivecount + 1;
                 }
             }
             //////////////////////////////////////////////////////////////////////    
             
           //REGARDE LES MONSTRES VIVANTS ET MET A JOUR LE NOMBRE/////////////////
            monsteralive = 0;
            
            for(int i =0;i<= alien.length-1;i++)
            {
                if (alien[i].alive)
                    monsteralive = monsteralive + 1;
            }
            
            Space.UpdateAlive(monsteralive);
            
            ///////////////////////////////////////////////////////////////////////
            
            
            int randomNum;
            if(monsteralive !=0)
            randomNum = getRandom(alive,alivecount);
            else
                randomNum = 0;
            
             
             //LANCER DE BOMBES////////////////////////////////////////////////////
             if(bomb)
             {
                  if (!oncebomb)
                {
                     bombdrop.x = alien[randomNum].x;
                     bombdrop.y = alien[randomNum].y;
                  
                     oncebomb = true;
                }
                   Space.SupprBullet(bombdrop.x, bombdrop.y);
                   bombdrop.y = bombdrop.y +5;
               if(bombdrop.y >= 200 || hitboxbomb(Space,bombdrop,p) || hitboxprotection(Space,bombdrop))
                {
                    Space.SupprBullet(bullet.x, bullet.y);
                    oncebomb = false;
                    bomb = false;                  
                }
                  
             } 
                 
             if(bomb)
             Space.DrawBullet(bombdrop.x,bombdrop.y, colore);
             
             ///////////////////////////////////////////////////////////////////////
             
             
            //CONTROLES DU JOUEUR/////////////////////////////////////////////////// 
            switch (Space.toucheTapee())
            {
                case 1: 
                    if(p.canmoveleft)
                    {
                         Space.SupprPlayer(p.x, p.y);
                         p.x = p.x -5;
                    }
                        break;
                        
                case 2: 
                    if(p.canmoveright)
                    {
                         Space.SupprPlayer(p.x, p.y);
                         p.x = p.x +5;
                    }
                break;
                case 0: 
                    fire = true;
                   
                break;
            }
            ////////////////////////////////////////////////////////////////////////
            
          
            
            if(monsteralive == 0)       //SI IL N Y A PLUS DE MONSTRE NIVEAU SUIVANT
                finish = true;
            
            
            
            if(p.live == 0 || alienreached(alien)) //SI LE JOUEUR N A PLUS DE VIE OU QUE LES ALIENS ONT GAGNE , GAME OVER
            {
                gameover = true;
                finish = true;
            }
                
            
            bomb = true;
            

        } 
        sauvegarderscore(score,nom);
        
        if(!gameover)
        Start(diffulty,niveau+1,colorp,colore, nom);
        else
            Space.afficheMessage(niveau, nom);
        
      
    }
    
}
