/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvade;

import java.awt.Color;
import static spaceinvade.SpaceInvade.hitbox;
import static spaceinvade.SpaceInvade.score;
import static spaceinvade.SpaceInvade.vitesseniveau;

/**
 *
 * @author PIERRE
 */
public class SpaceInvadeM {
    
      static boolean hitboxm(InterfaceSpaceInvaders Space,SpaceInvade.Alien alien, SpaceInvade.Bullet bullet)
       {
           int alienw = 8;
           int alienh = 9;
           return (bullet.x >= alien.x - alienw && bullet.x <= alien.x+alienw) && (bullet.y >= alien.y && alien.y+alienh >= bullet.y) && (alien.alive);
       }
       

       
       	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////  REGARDE SI UN ALIEN TOUCHE LE JOUEUR  ///////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static boolean hitboxbombm(InterfaceSpaceInvaders Space,SpaceInvade.Bomb bomb, SpaceInvade.Player play)
       {
           int playerw = 10;
           int playerh = 9;
           
           return (bomb.x >= play.x - 2 && bomb.x <= play.x+playerw) && (bomb.y >= play.y && play.y+playerh >= bomb.y);
       }
         private static String gagnant;
    
        public static void StartMulti(Color colorp, Color colore) throws InterruptedException { 
        

        
        SpaceInvade.Alien alien = new SpaceInvade.Alien(); //iNITIALISATION DES ALIENS
        
        boolean finish = false;
        
        boolean fire = false;
        boolean once = false;
        
        boolean bomb = false;
        boolean oncebomb = false;
        
        int x = 20;
        int y = 20;
       
       
        
        SpaceInvade.Player p = new SpaceInvade.Player();
        
        alien.x = x;
        alien.y = y;
        
        p.x = 95;                   //INITIALISATION DU JOUEUR
        p.y = 170;
        
        SpaceInvade.Bullet bullet = new SpaceInvade.Bullet();   //INITIALISATION DE LA BALLE
        
        SpaceInvade.Bomb bombdrop = new SpaceInvade.Bomb();     //INITIALISATION DE LA BOMBE
        
        InterfaceSpaceInvaders Space = new InterfaceSpaceInvaders(205, 200,Color.BLACK);

        
        int sleep = 40; //VITESSE DU NIVEAU
        
        
        
        
        while(!finish)  //BOUCLE DE JEU
        {
            
            //DESSINE LES ALIENS/////////////////////////////
              Space.DrawAlien(alien.x, alien.y,colore);
            
            /////////////////////////////////////////////////
            
            Space.DrawPlayer(p.x, p.y, colorp); //DESSINE LE JOUEUR
            
               
         
            
            //DETECTION DES BORDS POUR LE JOUEUR////////////////////////////////////////////////
            
            switch (p.x) {
                case 5:
                    p.canmoveleft = false;
                    break;
                case 185:
                    p.canmoveright = false;
                    break;
                default:
                    p.canmoveright = true;
                    p.canmoveleft = true;
                    break;
            }
            //////////////////////////////////////////////////////////////////////////////////////
            
       //DETECTION DES BORDS POUR LE JOUEUR////////////////////////////////////////////////
            
            switch (alien.x) {
                case 5:
                    alien.canmoveleft = false;
                    break;
                case 185:
                    alien.canmoveright = false;
                    break;
                default:
                    alien.canmoveright = true;
                    alien.canmoveleft = true;
                    break;
            }
            //////////////////////////////////////////////////////////////////////////////////////
            
            //ACTIVATION DU TIR/////////////////////////////////////////////////////////////////
            if (fire)
            { 
                 Thread.sleep(sleep);
                if (!once) //on initialise le depart du tir
                {
                     bullet.x = p.x;
                     bullet.y = p.y;
                   //  play("src\\spaceinvade\\shoot.wav");
                     once = true;
                }
                
                Space.SupprBullet(bullet.x, bullet.y);   //mouvement de la balle
                bullet.y = bullet.y -5;
                
                if(bullet.y <= 0 )//si la balle est hors champs ou touche un alien
                {
                    Space.SupprBullet(bullet.x, bullet.y);
                    once = false;
                    fire = false;                  
                } 
                  if ( hitboxm(Space,alien,bullet))
                  {
                                        finish = true; 
                                        gagnant = "Canon";
                  }

                
            }
            //////////////////////////////////////////////////////////////////////////////////////
            
            
                
             if (fire)
              Space.DrawBullet(bullet.x,bullet.y,colorp); //mouvement de la balle
             
             
           
            
             
             //LANCER DE BOMBES////////////////////////////////////////////////////
             if(bomb)
             {
                                     Thread.sleep(sleep);
                  if (!oncebomb)
                {
                     bombdrop.x = alien.x;
                     bombdrop.y = alien.y;
                  
                     oncebomb = true;
                }
                   Space.SupprBullet(bombdrop.x, bombdrop.y);
                   bombdrop.y = bombdrop.y +5;
               if(bombdrop.y >= 200)
                {
                    Space.SupprBullet(bullet.x, bullet.y);
                    oncebomb = false;
                    bomb = false;                  
                }
               if (hitboxbombm(Space,bombdrop,p))
               {
                                     finish = true; 
                                     gagnant = "Alien";
               }

                  
             } 
                 
             if(bomb)
             Space.DrawBullet(bombdrop.x,bombdrop.y, colore);
             
             ///////////////////////////////////////////////////////////////////////
             
             
            //CONTROLES DU JOUEUR/////////////////////////////////////////////////// 
            switch (Space.toucheTapee())
            {
                case 1: 
                    if(p.canmoveleft)
                    {
                         Space.SupprPlayer(p.x, p.y);
                         p.x = p.x -5;
                    }
                    break;
                 case 6:  
                      if(alien.canmoveleft)
                    {
                         Space.SupprAlien(alien.x, alien.y);
                         alien.x = alien.x -5;
                    }
                        break;
                        
                case 2: 
                    if(p.canmoveright)
                    {
                         Space.SupprPlayer(p.x, p.y);
                         p.x = p.x +5;
                    }
                                    break;
                    case 7:  
                                        if(alien.canmoveright)
                    {
                         Space.SupprAlien(alien.x, alien.y);
                         alien.x = alien.x +5;
                    }
                break;
                case 0: 

                    fire = true;
                                    break;
                     case 8: 
                      bomb = true;
                   
                break;
            }
            ////////////////////////////////////////////////////////////////////////
                
           
            

        } 
                 Space.afficheMessage2(gagnant);
        
      
    }

    
}
